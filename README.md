# Bornfight Frontend Task

## Installation

```
npm install
```

## Start Development Server

```
npm start
```

## Build Production Version

```
npm run build
```

You can also use `yarn` instead of `npm`.

## NOTES

* Bornfight Frontend Task is a project created with Chisel. Please check out Chisel documentation at [https://www.getchisel.co/docs/](www.getchisel.co/docs/) for more information. Left pretty much everything as-is when I generated the project.
* Sass, Gulp, Webpack & Twig. 
* Used ITCSS.
* Vanilla JS only, no plugins used, coupled with ESLint strict performance enhancing ESLint config (no loops, no runtime generators, no reassigment of funtion parameters, no calls before definitions, etc.).
* In JavaScript we are assuming best case scenarios, only necessary checks have been done. Also, event listeners haven't been removed but they're not firing since there are no additional calls to binding events. Added console.log output so we can track firing of animations more easily.
* _Responsive-ish_ - scaled some elements but don't expect much, best to view it at native resolution (1920px);
* Slice is done but didn't have enough time to finish the animations.

## CASE FOR IMPROVEMENTS
There is a lot of room for improvements since I underestimated the time I would need to finish the assigment.

* CSS
	- class naming coud've been improved (ITCSS + BEM is always tricky :))
	- take advantage of gulp/postcss-ready animation packages
* JS
	- since I'm not familiar with how ScrollMagic and GSAP libraries work I decided to build out every animation functionality from scratch. Basically, what we are doing is queuing animations by using event listeners to add and remove css classes. 

## FEEDBACK
I would appreciate any and all feedback from you, not about the slicing part, naming conventions & project architecture & structure but rather on tweening & queueing animations. How would you have done it?
