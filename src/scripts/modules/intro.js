/* eslint-disable no-use-before-define */

const intro = () => {
	// declare necessary vars
	const header = document.querySelector('.c-header');
	const introSection = document.querySelector('.c-section-intro');
	const IntroTitleParts = [
		...document.querySelectorAll('.c-section-intro__title'),
	];
	const IntroSubtitleParts = [
		...document.querySelectorAll('.c-section-intro__subtitle'),
	];
	const introOverlay = document.querySelector('.c-section-intro__overlay');
	const bigTitleSection = document.querySelector('.c-section-big-title');
	const bigTitleParts = bigTitleSection.querySelectorAll(
		'.c-section-big-title__title',
	);
	const bigTitleSectionSubtitle = bigTitleSection.querySelector(
		'.c-section-big-title__subtitle',
	);

	// Wrap every letter in the title into a span and then animate them
	wrapLetters(IntroTitleParts).then(animateIntroTitle(IntroTitleParts));
	// wrap title letters in big title section, activate transition at a later time
	wrapLetters(bigTitleParts);

	header.addEventListener(
		'animationend',
		() => {
			console.log(`
				FINISH animating HEADER
				=================================
				START ANIMATING BIG TITLE SECTION
			`);
			introSection.classList.add('u-hidden-visually');
			bigTitleSection.classList.remove('c-section-big-title--abs');
			animateBigTitle(bigTitleParts);
		},
		false,
	);

	function toggleAnimationClasses(
		element,
		animation = 'u-animate-fade-in-up',
		speed,
		delayClass,
	) {
		element.classList.add('animated', animation);
		if (speed) element.classList.add(speed);
		if (delayClass) element.classList.add(delayClass);

		if (element.classList.contains('u-hidden-visibility'))
			element.classList.remove('u-hidden-visibility');
	}

	// wrap letters into span tags and return a promise once done
	function wrapLetters(textElements) {
		return new Promise((resolve, reject) => {
			textElements.forEach((el, i) => {
				console.log(
					`START separating intro title into SPANs containing single letters! (row ${i +
						1})`,
				);
				const textWrapper = el;
				textWrapper.innerHTML = textWrapper.textContent.replace(
					/\S/g,
					"<span class='js-letter u-hidden-visibility'>$&</span>",
				);

				if (el.classList.contains('u-hidden-visibility'))
					el.classList.remove('u-hidden-visibility');

				// lets be sure that our DOM was converted before we start animating
				if (i === textElements.length - 1) {
					console.log('Finished!');
					resolve();
				}
			});
			reject();
		});
	}

	// animate intro subtitle with a counter
	function animateIntroSubtitle(elements) {
		[...elements].forEach((el, i) => {
			console.log(`START animating intro subtitle row ${i + 1}`);
			toggleAnimationClasses(el, 'u-animate-fade-in-up', 'fast');

			// animate year counter
			if (
				el.classList.contains('js-animation-counter') &&
				el.getAttribute('data-count-from') &&
				el.getAttribute('data-count-to') &&
				el.getAttribute('data-count-to') > el.getAttribute('data-count-from')
			) {
				console.log('START animating intro subtitle year counter');
				// we declare the element to avoid eslint errors
				const element = el;
				let countFrom = element.getAttribute('data-count-from');
				const countTo = element.getAttribute('data-count-to');
				const interval = setInterval(() => {
					if (countFrom <= countTo) {
						// eslint-disable-next-line no-plusplus
						element.querySelector('span').innerHTML = countFrom++;
					} else {
						console.log('FINISH animating intro subtitle year counter');
						clearInterval(interval);
					}
				}, 1300 / (countTo - countFrom));
			}

			// when it finishes animating add another custom animation (move it up)
			el.addEventListener(
				'animationend',
				() => {
					// slide white background in && header
					if (i === 0) {
						console.log('START animating intro overlay');
						toggleAnimationClasses(introOverlay, 'u-animate-intro-overlay');

						console.log('START animating header');
						toggleAnimationClasses(
							header,
							'u-animate-fade-in-down',
							'',
							'u-animate-delay-1',
						);
					}
				},
				false,
			);
		});
	}

	// animate intro title function
	function animateIntroTitle(textElements) {
		// iterate over into title dom elements
		textElements.forEach((el, idx) => {
			const textWrapper = el;
			const textWrapperIndex = idx;

			console.log(`START animating intro title row ${textWrapperIndex + 1}`);

			setTimeout(() => {
				const letters = [...textWrapper.querySelectorAll('.js-letter')];

				// iterate over every letter inside each title element
				letters.forEach((letter, i) => {
					setTimeout(() => {
						// start second animation just before first intro title row finishes animating
						// eyeballing this, since we don't use animation libraries to better deal with this
						if (textWrapperIndex === 1 && i === letters.length - 6) {
							letter.addEventListener(
								'animationend',
								() => {
									console.log(
										'Animation of first row ended, START animating intro subtitle!',
									);
									animateIntroSubtitle(IntroSubtitleParts);
								},
								false,
							);
						} else if (
							textWrapperIndex + 1 === textElements.length &&
							i + 1 === letters.length
						) {
							letter.addEventListener(
								'animationend',
								() => {
									console.log(
										`Animation of last intro title letter finished, moving up title & subtitle! (row ${textWrapperIndex +
											1})`,
									);
									toggleAnimationClasses(
										textWrapper.parentNode,
										'u-animate-slide-out-up',
										'slow',
									);
								},
								false,
							);
						}

						toggleAnimationClasses(letter, 'u-animate-fade-in-up', 'fast');
					}, 200 * i);
				});
			}, 500 * textWrapperIndex);
		});
	}

	// animate big title
	function animateBigTitle(textElements) {
		// iterate over into title dom elements
		textElements.forEach((el, idx) => {
			const textWrapper = el;
			const textWrapperIndex = idx;

			console.log(`START animating big title row ${textWrapperIndex + 1}`);

			setTimeout(() => {
				const letters = [...textWrapper.querySelectorAll('.js-letter')];

				// iterate over every letter inside each title element
				letters.forEach((letter, i) => {
					setTimeout(() => {
						if (textWrapperIndex === 1 && i === letters.length - 1) {
							// when last letter in second row is done animating animate the subtitle
							letter.addEventListener('animationend', () => {
								console.log(
									'Animation of big title ended, START animating intro subtitle!',
								);

								toggleAnimationClasses(
									bigTitleSectionSubtitle,
									'u-animate-simple-up',
								);
							});
						}

						toggleAnimationClasses(letter, 'u-animate-fade-in-up', 'fast');
					}, 200 * i);
				});
			}, 500 * textWrapperIndex);
		});
	}
};

export default intro;
