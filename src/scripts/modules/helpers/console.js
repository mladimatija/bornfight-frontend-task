const origlog = console.log;
console.log = function(obj, ...placeholders) {
	function addZero(i) {
		let out = i;
		if (i < 10) out = `0${i}`;
		return out;
	}

	const today = new Date();
	const date = `${today.getFullYear()}-${today.getMonth() +
		1}-${today.getDate()}`;
	const time = `${addZero(today.getHours())}:${addZero(
		today.getMinutes(),
	)}:${addZero(today.getSeconds())}`;
	const dateTime = `${date} ${time}`;

	if (typeof obj === 'string') placeholders.unshift(`${dateTime} ${obj}`);
	else {
		// This handles console.log( object )
		placeholders.unshift(obj);
		placeholders.unshift(`${dateTime} %j`);
	}

	origlog.apply(this, placeholders);
};
